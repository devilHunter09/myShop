import { Router } from 'preact-router';

import Header from './header';
import Home from '../routes/home';

const App = () => (
	<div class="container" id="app">
		<div class="row">
			<Header />

		</div>
		<div class="row">
			<Router>
				<Home path="/" />
			</Router>
		</div>
	</div>

)

export default App;
